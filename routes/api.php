<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Http\Controllers\AccessTokenController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/auth/login', [AuthController::class, 'login'])
    ->name('login');

Route::group(['prefix' => 'users'], function () {
    Route::post('/', [UserController::class, 'store'])
        ->name('users.store');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('/', [UserController::class, 'get'])
            ->name('users.get');

        Route::put('/{user}', [UserController::class, 'update'])
            ->name('users.update');
    });
});

Route::group(
    ['middleware' => 'auth:api', 'prefix' => 'employees'],
    function () {
        Route::post('/import', [\App\Http\Controllers\EmployeeController::class, 'import'])
            ->name('employees.import');

        Route::get('/', [\App\Http\Controllers\EmployeeController::class, 'get'])
            ->name('employees.get');

        Route::post('/', [\App\Http\Controllers\EmployeeController::class, 'store'])
            ->name('employees.store');

        Route::get('/{employee}', [\App\Http\Controllers\EmployeeController::class, 'show'])
            ->name('employees.show');

        Route::delete('/{employee}', [\App\Http\Controllers\EmployeeController::class, 'destroy'])
            ->name('employees.destroy');
    }
);
