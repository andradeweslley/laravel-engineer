<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = Passport::actingAs(
            User::factory()->create(),
            ['create-servers']
        );
    }

    public function testItGetsAllUsersWithAuthUser(): void
    {
        User::factory(3)->create();

        $users = User::all();

        $this->get(route('users.get'))
            ->assertOk()
            ->assertJson(
                $users->map(function ($user) {
                    return [
                        'id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email,
                    ];
                })->toArray()
            );
    }

    public function testItCreatesNewUser(): void
    {
        $user = User::factory()->makeOne();

        $user->makeVisible('password');

        $this->post(route('users.store'), $user->toArray())
            ->assertCreated()
            ->assertJson([
                'message' => 'Usuário criado com sucesso',
            ]);
    }

    public function testItUpdatesUser(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        $data = [
            'name' => 'Tester',
        ];

        $this->put(
            route('users.update', ['user' => $user]),
            $data
        )
            ->assertOk()
            ->assertJson([
                'message' => 'Usuário atualizado com sucesso',
            ]);

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'name' => $data['name'],
        ]);
    }
}
