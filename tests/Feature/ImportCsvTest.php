<?php

namespace Tests\Feature;

use App\Jobs\ImportEmployeesFromCsvJob;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Testing\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Passport;
use League\Csv\Writer;
use Tests\TestCase;

class ImportCsvTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = Passport::actingAs(
            User::factory()->create(),
            ['create-servers']
        );
    }

    public function testItCreatesFromCsvFile(): void
    {
        Queue::fake();

        $employee = Employee::factory()
            ->make()
            ->makeHidden(['user_id', 'id'])
            ->toArray();

        $data = [array_keys($employee), $employee];
        $file = $this->buildAndSaveCsv($data);

        $this->post(route('employees.store'), ['file' => $file])
            ->assertCreated()
            ->assertJson([
                'message' => 'Lista de funcionários adicionado na fila para processamento',
                'data' => [
                    'file' => $file->getClientOriginalName(),
                ],
            ]);

        Queue::assertPushed(ImportEmployeesFromCsvJob::class, function ($job) use ($employee) {
            $job->handle();

            return $job->response['successes'][0]->email === $employee['email'];
        });
    }

    public function testItUpdatesFromCsvFile(): void
    {
        Queue::fake();

        $employee = Employee::factory()
            ->create()
            ->makeHidden(['user_id', 'id'])
            ->toArray();

        $employee['name'] = 'Tester';

        $data = [array_keys($employee), $employee];
        $file = $this->buildAndSaveCsv($data);

        $this->post(route('employees.store'), ['file' => $file])
            ->assertCreated()
            ->assertJson([
                'message' => 'Lista de funcionários adicionado na fila para processamento',
                'data' => [
                    'file' => $file->getClientOriginalName(),
                ],
            ]);

        Queue::assertPushed(ImportEmployeesFromCsvJob::class, function ($job) use ($employee) {
            $job->handle();

            return $job->response['successes'][0]->name === $employee['name'];
        });
    }

    public function testItCannotUpdateWhenErrorInCsvFile(): void
    {
        Queue::fake();

        $employee = Employee::factory()
            ->create()
            ->makeHidden(['user_id', 'id'])
            ->toArray();

        $employee['start_date'] = '2023-02-29';

        $data = [array_keys($employee), $employee];
        $file = $this->buildAndSaveCsv($data);

        $this->post(route('employees.store'), ['file' => $file])
            ->assertCreated()
            ->assertJson([
                'message' => 'Lista de funcionários adicionado na fila para processamento',
                'data' => [
                    'file' => $file->getClientOriginalName(),
                ],
            ]);

        Queue::assertPushed(ImportEmployeesFromCsvJob::class, function ($job) {
            $job->handle();

            return isset($job->response['errors'][0]['details']['start_date']);
        });
    }

    /**
     * Creates a CSV File
     *
     * @param array<int, array<string, int|string>> $data
     *
     * @return File
     */
    private function buildAndSaveCsv(array $data): File
    {
        Storage::fake('uploads');

        $csv = Writer::createFromString();
        $csv->insertAll($data);

        return UploadedFile::fake()
            ->createWithContent('test.csv', $csv->toString());
    }
}
