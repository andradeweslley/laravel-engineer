<?php

namespace Tests\Feature;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class EmployeeControllerTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = Passport::actingAs(
            User::factory()->create(),
            ['create-servers']
        );
    }

    public function testItGetsAllEmployeesWithAuthUser(): void
    {
        /** @var Employee $employees */
        $employees = Employee::factory(3)->create([
            'user_id' => $this->user->id
        ]);

        $this->get(route('employees.get'))
            ->assertOk()
            ->assertJson(
                $employees->toArray()
            );
    }

    public function testItCannotGetsEmployeesFromAnotherUser(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        Employee::factory(3)->create([
            'user_id' => $user->id
        ]);

        $this->get(route('employees.get'))
            ->assertOk()
            ->assertExactJson([]);
    }

    public function testItShowsEmployeeWithAuthUser(): void
    {
        /** @var Employee $employee */
        $employee = Employee::factory()->create([
            'user_id' => $this->user->id
        ]);

        $this->get(route('employees.show', [$employee->id]))
            ->assertOk()
            ->assertJson($employee->toArray());
    }

    public function testItCannotShowsEmployeeFromAnotherUser(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var Employee $employee */
        $employee = Employee::factory()->create([
            'user_id' => $user->id
        ]);

        $this->get(route('employees.show', [$employee->id]))
            ->assertForbidden()
            ->assertSee('This action is unauthorized.');
    }

    public function testItCreatesEmployeeFromArray(): void
    {
        /** @var Employee $employee */
        $employee = Employee::factory()->makeOne();

        $this->json(
            'POST',
            route('employees.store'),
            $employee->toArray()
        )
            ->assertCreated()
            ->assertJson([
                'message' => 'Funcionário criado com sucesso'
            ]);
    }

    public function testItDeletesEmployeeWithAuthUser(): void
    {
        /** @var Employee $employee */
        $employee = Employee::factory()->create([
            'user_id' => $this->user->id
        ]);

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id
        ]);

        $this->delete(route('employees.destroy', [$employee->id]))
            ->assertOk()
            ->assertExactJson([
                'Success' => true
            ]);

        $this->assertDatabaseMissing('employees', [
            'id' => $employee->id
        ]);
    }

    public function testItCannotDeletesEmployeeFromAnotherUser(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var Employee $employee */
        $employee = Employee::factory()->create([
            'user_id' => $user->id
        ]);

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id,
            'user_id' => $user->id
        ]);

        $this->delete(route('employees.destroy', [$employee->id]))
            ->assertForbidden()
            ->assertSee('This action is unauthorized.');

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id,
            'user_id' => $user->id
        ]);
    }
}
