<?php

namespace App\Jobs;

use App\Http\Requests\EmployeeRequest;
use App\Models\Employee;
use App\Models\User;
use App\Notifications\EmployeesCreationNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Validator;
use League\Csv\Reader;
use League\Csv\Statement;

class ImportEmployeesFromCsvJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    const RELATIVE_PATH = 'app/imports/employees/';

    /**
     * Saves return of each line processed
     *
     * @var array<string, array<\App\Models\Employee>|array<int, array<string, array<int, string>>>>
     */
    public array $response;

    private string $storagePath;
    private User $user;

    /**
     * Create a new job instance.
     */
    public function __construct(string $fileName, User $user)
    {
        $this->user = $user;
        $this->storagePath = storage_path(self::RELATIVE_PATH . $fileName);
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $successes = [];
        $errors = [];
        $rules = (new EmployeeRequest())->store();

        $reader = Reader::createFromPath($this->storagePath);

        $reader->setDelimiter(',');
        $reader->setHeaderOffset(0);

        $records = Statement::create()->process($reader);

        foreach ($records as $key => $record) {
            $validator = Validator::make($record, $rules);

            if ($validator->fails()) {
                $errors[] = [
                    'line' => ($key + 1),
                    'details' => $validator->errors()->toArray()
                ];
            } else {
                $data = $validator->validated();

                $successes[] = Employee::updateOrCreate(
                    ['document' => $data['document'], 'user_id' => $this->user->id],
                    $data
                );
            }
        }

        $this->response = [
            'successes' => $successes,
            'errors' => $errors,
        ];

        $this->user->notify(new EmployeesCreationNotification($this->response));
    }
}
