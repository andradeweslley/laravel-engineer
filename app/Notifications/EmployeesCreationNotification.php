<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EmployeesCreationNotification extends Notification
{
    use Queueable;

    /**
     * Array that contains status of each line processed
     *
     * @var array<int, string>
     */
    private array $responseArray;

    /**
     * Create a new notification instance.
     *
     * @param array<string, array<\App\Models\Employee>|array<int, array<string, array<int, string>>>> $responses
     */
    public function __construct(array $responses)
    {
        $this->formatResponses($responses);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage())
            ->from('test@test.com', 'Test')
            ->subject('Planilha processada')
            ->line('Planilha processada. Segue abaixo o status de processamento de cada linha')
            ->lines($this->responseArray);
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [];
    }

    /**
     * Format responses to Array of Strings
     *
     * @param array<string, array<\App\Models\Employee>|array<int, array<string, array<int, string>>>> $responses
     */
    private function formatResponses(array $responses): void
    {
        if (!empty($responses['errors'])) {
            $this->responseArray[] = "Ocorreram os seguintes erros:";

            foreach ($responses['errors'] as $error) {
                $this->responseArray[] = "---------------------";
                $this->responseArray[] = "Linha: {$error['line']}.";

                $errors = array_values($error['details']);

                foreach ($errors as $error) {
                    $this->responseArray[] = $error;
                }

                $this->responseArray[] = "";
            }

            $this->responseArray[] = "";
        }

        if (!empty($responses['successes'])) {
            $this->responseArray[] = "Os seguintes funcionários foram cadastrados:\n";

            foreach ($responses['successes'] as $employee) {
                $this->responseArray[] = "---------------------";
                $this->responseArray[] = "Funcionário: {$employee->name} ({$employee->email}).";
                $this->responseArray[] = "";
            }
        }
    }
}
