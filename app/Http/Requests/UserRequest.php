<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array<int, string>>
     */
    public function rules(): array
    {
        return match ($this->method()) {
            'PUT', 'PATCH' => $this->update(),
            default => $this->store(),
        };
    }

    /**
     * Validation rules to store request
     *
     * @return array<string, array<int, string>>
     */
    private function store(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email'],
            'password' => ['required', Password::min(5)]
        ];
    }

    /**
     * Validation rules to update request
     *
     * @return array<string, array<int, string>>
     */
    private function update(): array
    {
        return [
            'name' => ['string', 'max:255'],
            'password' => [Password::min(5)]
        ];
    }
}
