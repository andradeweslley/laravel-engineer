<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, array<int, string>>
     */
    public function rules(): array
    {
        return match ($this->method()) {
            'PUT', 'PATCH' => $this->update(),
            default => $this->store(),
        };
    }

    /**
     * Validation rules to store request
     *
     * @return array<string, array<int, string>>
     */
    public function store(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email'],
            'document' => ['required', 'numeric'],
            'city' => ['required', 'string', 'max:255'],
            'state' => ['required', 'string', 'max:255'],
            'start_date' => ['required', 'date', 'date_format:Y-m-d'],
        ];
    }

    /**
     * Validation rules to update request
     *
     * @return array<string, array<int, string>>
     */
    public function update(): array
    {
        return [
            'name' => ['string', 'max:255'],
            'email' => ['email'],
            'document' => ['numeric'],
            'city' => ['string', 'max:255'],
            'state' => ['string', 'max:255'],
            'start_date' => ['date', 'date_format:Y-m-d'],
        ];
    }
}
