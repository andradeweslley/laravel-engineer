<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeImportRequest;
use App\Http\Requests\EmployeeRequest;
use App\Jobs\ImportEmployeesFromCsvJob;
use App\Models\Employee;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{
    public function get(): JsonResponse
    {
        return response()->json(
            Employee::where('user_id', auth()->user()->id)->get()
        );
    }

    public function store(EmployeeRequest $request): JsonResponse
    {
        $employee = new Employee();

        $employee->fill($request->validated());

        $employee->user_id = $request->user()->id;

        $employee->save();

        return response()->json([
            'message' => 'Funcionário criado com sucesso',
            'data' => $employee,
        ], 201);
    }

    public function import(EmployeeImportRequest $request): JsonResponse
    {
        $fileName = time() . "_" . $request->file('file')->getClientOriginalName();

        $request->file('file')->storeAs('imports/employees/', $fileName);

        ImportEmployeesFromCsvJob::dispatch($fileName, Auth::user());

        return response()->json([
            'message' => 'Lista de funcionários adicionado na fila para processamento',
            'data' => [
                'file' => $request->file('file')->getClientOriginalName(),
            ],
        ], 201);
    }

    public function show(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json(
            $employee
        );
    }

    public function destroy(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json([
            'Success' => $employee->delete()
        ]);
    }
}
