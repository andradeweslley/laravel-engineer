<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Attempts login
     *
     * @param AuthRequest $request
     *
     * @return JsonResponse
     */
    public function login(AuthRequest $request): JsonResponse
    {
        if (!Auth::attempt($request->validated())) {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'Usuário ou senha inválidos',
            ], 401);
        }

        $token = Auth::user()->createToken(config('auth.passport.token'))->accessToken;

        return response()->json([
            'token' => $token,
        ]);
    }
}
