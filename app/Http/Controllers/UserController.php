<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    public function get(): JsonResponse
    {
        $users = User::all();

        return response()->json($users);
    }

    public function store(UserRequest $request): JsonResponse
    {
        $data = $request->validated();

        if (User::where('email', $data['email'])->exists()) {
            return response()->json([
                'message' => 'E-mail já cadastrado',
            ], 400);
        }

        $user = User::create($data);

        return response()->json([
            'message' => 'Usuário criado com sucesso',
            'data' => $user,
        ], 201);
    }

    public function update(UserRequest $request, User $user): JsonResponse
    {
        try {
            $user->fill($request->validated());
            $user->save();

            return response()->json([
                'message' => 'Usuário atualizado com sucesso',
            ]);
        } catch (Exception $e) {
            return response()->json([
                'message' => 'Erro ao atualizar usuário',
            ]);
        }
    }
}
